# contrax

A secure document exchange system.

## Purpose

I designed this system when I began offically freelancing as a developer, in order to facilitate the secure and organized exchange of contracts between me and my clients (i.e. NDAs, rate agreements, etc.).

## Implementation

The system consists of two components: the user-facing website, and the backend API which coordinates requests and securely stores user data.

This repository contains the back-end (server/API) component, and its implementation is described below:

1. Create a [Heroku](https://heroku.com) account.
2. Follow the steps on [this guide](https://devcenter.heroku.com/articles/getting-started-with-go?singlepage=true) to deploy an instance with this API.
3. Deploy a MongoDB atlas cluster with [this guide](https://docs.atlas.mongodb.com/getting-started/).
4. Ensure that all IP addresses are whitelisted to account for the serverless nature of the Heroku instance. In lieu of IP address protection, ensure that all generated user passwords are strong and not prone to brute-force attacks. The autogenerate feature provided by MongoDB should be sufficient.
5. Create a new database on the cluster with a name of your choice.
6. Create two collections on that database: `users` and `files`.
7. Create a user on the cluster with the username `golang` and permissions to `readWrite` the `contrax` database's `users` and `files` collections.
8. Ensure that the user is authenticated via password (SCRUM). Again, make sure that the password is strong.
9. In your Heroku instance, create a new environment variable, titled `MONGODB_PASSWORD` and storing the password for the `golang` user.
10. Create three other environment variables: `MONGODB_DOMAIN` to store the domain of your MongoDB cluster (located after the @ in your connection URL), `MONGODB_DATABASE` to store the name of the database, and `ORIGIN` to store the URL of the front-end instance.
11. For each contrax user, insert a new document into the `users` collection of the database with the following format:

```json
{"Username":"janedoe"}
```

All necessary fields will be populated into the newly inserted document when the user actually registers for their account.

## License

See [LICENSE](./LICENSE).
