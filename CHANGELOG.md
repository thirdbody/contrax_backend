# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- `MONGODB_DATABASE` environment variable requirement is now mentioned in README.md

### Changed
- Fixed CHANGELOG unreleased link to go from `v1.1.0` to `master`

### Removed
- N/A

## [1.1.0] - 2020-06-29
### Added
- Created CHANGELOG.md
- Created CONTRIBUTING.md
- Allowing files with recipient marked as wildcard (*) to be seen and downloaded by any user
- Added `GetFilesD` function to allow `GetFiles` to be called with a bson.D query

### Changed
- Using environment variable to determine CORS origin header
- `GetFileNamesAsJSON` is now `GetFileNames`; returns `[]string` instead of marshalled JSON to allow slice operations to be performed

### Removed
- `Vary: Origin` header; unnecessary due to use of constant environment variable (minor breaking change; please update your env-vars)
- Call to `client.Database` now relies on environment variable `MONGODB_DATABASE` (minor breaking change; please update your env-vars)

## [1.0.0] - 2020-06-28
### Added
- Initial project version

### Changed
- N/A

### Removed
- N/A

[Unreleased]: https://gitlab.com/myl0g/contrax_backend/compare/v1.1.0...master
[1.1.0]: https://gitlab.com/myl0g/contrax_backend/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/myl0g/contrax_backend/compare/v0.0.1...v1.0.0
